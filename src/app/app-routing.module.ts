import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginGuardService } from './guards/login-guard.service';
import { AuthGuardService } from './guards/auth-guard.service';
import { NetworkGuard } from './guards/network.guard';
import { NetworkconnectedGuard } from './guards/networkconnected.guard';

const routes: Routes = [
  { path: '', loadChildren: './pages/login/login.module#LoginPageModule', canActivate: [LoginGuardService, NetworkGuard] },
  { path: 'forgotpassword', loadChildren: './pages/forgotpassword/forgotpassword.module#ForgotpasswordPageModule', canActivate: [NetworkGuard] },
  { path: 'registration', loadChildren: './pages/registration/registration.module#RegistrationPageModule', canActivate: [NetworkGuard] },
  { path: 'accountverification', loadChildren: './pages/accountverification/accountverification.module#AccountverificationPageModule', canActivate: [NetworkGuard] },
  { path: 'newtorkdisconnected', loadChildren: './pages/newtorkdisconnected/newtorkdisconnected.module#NewtorkdisconnectedPageModule', canActivate: [NetworkconnectedGuard] },
  { path: 'home', loadChildren: './pages/home/home.module#HomePageModule', canActivate: [NetworkGuard] },
  { path: 'documents', loadChildren: './pages/documents/documents.module#DocumentsPageModule', canActivate: [AuthGuardService, NetworkGuard] },
  { path: 'profile', loadChildren: './pages/profile/profile.module#ProfilePageModule', canActivate: [AuthGuardService, NetworkGuard] },
  { path: 'verifyupdates', loadChildren: './pages/verifyupdates/verifyupdates.module#VerifyupdatesPageModule', canActivate: [AuthGuardService, NetworkGuard] },
  { path: 'mygarage', loadChildren: './pages/garage/mygarage/mygarage.module#MygaragePageModule', canActivate: [AuthGuardService, NetworkGuard] },
  { path: 'cardetails', loadChildren: './pages/garage/cardetails/cardetails.module#CardetailsPageModule', canActivate: [AuthGuardService, NetworkGuard] },
  { path: 'viewplan', loadChildren: './pages/garage/viewplan/viewplan.module#ViewplanPageModule',  canActivate: [AuthGuardService, NetworkGuard] },
  { path: 'getquote', loadChildren: './pages/getquotetool/getquote/getquote.module#GetquotePageModule', canActivate: [NetworkGuard] },
  { path: 'carinsurance', loadChildren: './pages/getquotetool/carinsurance/carinsurance.module#CarinsurancePageModule', canActivate: [NetworkGuard] },
  { path: 'serviceplan', loadChildren: './pages/getquotetool/serviceplan/serviceplan.module#ServiceplanPageModule', canActivate: [NetworkGuard] },
  { path: 'quoteresult', loadChildren: './pages/getquotetool/quoteresult/quoteresult.module#QuoteresultPageModule', canActivate: [NetworkGuard] },
  { path: 'cart', loadChildren: './pages/getquotetool/cart/cart.module#CartPageModule' },
  { path: 'termsconditions', loadChildren: './pages/getquotetool/termsconditions/termsconditions.module#TermsconditionsPageModule', canActivate: [NetworkGuard] },
  { path: 'cartpersonaldetail', loadChildren: './pages/getquotetool/cartpersonaldetail/cartpersonaldetail.module#CartpersonaldetailPageModule', canActivate: [NetworkGuard] },
  { path: 'cartpaymentdetail', loadChildren: './pages/getquotetool/cartpaymentdetail/cartpaymentdetail.module#CartpaymentdetailPageModule', canActivate: [NetworkGuard] },
  { path: 'cartpaymentconfirmation', loadChildren: './pages/getquotetool/cartpaymentconfirmation/cartpaymentconfirmation.module#CartpaymentconfirmationPageModule' },
  { path: 'findcars', loadChildren: './pages/findcars/findcars.module#FindcarsPageModule', canActivate: [NetworkGuard] },
  { path: 'cardetail', loadChildren: './pages/findcars/cardetail/cardetail.module#CardetailPageModule', canActivate: [NetworkGuard] },
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
