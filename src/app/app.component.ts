import { Component, ViewChild } from '@angular/core';
import { NavController, Platform, IonRouterOutlet, ToastController, MenuController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Router }from '@angular/router';
import { NetworkService } from './services/network.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  @ViewChild(IonRouterOutlet) routerOutlet: IonRouterOutlet;

  lastTimeBackPress = 0;
  timePeriodToExit = 2000;
  public token;

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private router: Router,
    private navCtrl: NavController,
    public toastCtrl: ToastController,
    public menuCtrl: MenuController,
    private network: NetworkService) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.token = localStorage.getItem('token');
      this.network.checkConnection();
      this.splashScreen.hide();
      this.statusBar.show();
    });
  
    this.platform.backButton.subscribe(async () => {
      if (this.routerOutlet && this.routerOutlet.canGoBack()) {
        this.routerOutlet.pop();
      } else if (this.router.url === '/home' || this.router.url === '/' || this.router.url === '/newtorkdisconnected' ) {
        if (new Date().getTime() - this.lastTimeBackPress < this.timePeriodToExit) {
          navigator['app'].exitApp(); 
        } else {
          const toast = await this.toastCtrl.create({
            message: `Press back again to exit App`,
            duration: 2000
          });
          toast.present();
          this.lastTimeBackPress = new Date().getTime();
        }
      }
    });
  }

  closeMenu(){
    this.menuCtrl.close();
  }

  logout(){
    this.token = null;
    localStorage.clear();
    this.menuCtrl.close();
    this.navCtrl.navigateRoot(['/']);
  }
}   
 

