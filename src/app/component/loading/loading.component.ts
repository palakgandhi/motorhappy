import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { LoadingService, LoaderState } from '../../services/loading.service';

@Component({
  selector: 'app-loading',
  templateUrl: './loading.component.html',
  styleUrls: ['./loading.component.scss']
})
export class LoadingComponent implements OnInit {

  show = false;
  private subscription: Subscription;
  constructor(private loaderSvc: LoadingService) { }


  ngOnInit() {
    this.subscription = this.loaderSvc.loaderState.subscribe((state: LoaderState) => {
      setTimeout(() => this.show = state.show);
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
