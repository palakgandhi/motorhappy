import { Component, Output, EventEmitter, Input, forwardRef  } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'lookup',
  templateUrl: './lookup.component.html',
  styleUrls: ['./lookup.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => LookupComponent),
      multi: true
    }
  ]
})
export class LookupComponent implements ControlValueAccessor {

  query:string;
  modelValue = [];

  @Input() 
  entities: any[];

  @Input()
  label: any;

  @Input()
  placeholder: string;

  @Input()
  dataValue: string;

  @Input()
  preventItemAdd: boolean = false;

  @Output() 
  dataFn = new EventEmitter<any>();

  private propagateChange = (_: any) => { };

  writeValue(values: any) {
    if (values && values.length > 0) {
      this.modelValue = values;
    } else {
      this.modelValue = [];
    }
  }

  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }
  
  registerOnTouched(fn: any): void { }

  onsearch(event){
    if(this.query.length == 0){
      this.entities = [];
    }
    if(this.query.length < 3){
      return;
    }
    this.dataFn.emit(this.query);
  }

  onSelect(item){
    this.setDataValue(item);
  }

  private setDataValue(newItem) {
    this.modelValue.push(newItem);
    this.propagateChange(this.modelValue);
    this.query = '';
    this.entities.length = 0;
  }

  remove(idx) {
    this.modelValue.splice(idx, 1);
    this.propagateChange(this.modelValue);
  }


}
