import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { NetworkService } from '../services/network.service';

@Injectable({
  providedIn: 'root'
})
export class NetworkGuard implements CanActivate {

  constructor(public network: NetworkService, public router: Router) { }

  canActivate() : boolean {

    if(!this.network.checkConnection()) {
      this.router.navigate(['/newtorkdisconnected']);
      return false;
    } else {
      return true;
    }
  }
}
