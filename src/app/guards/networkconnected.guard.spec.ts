import { TestBed, async, inject } from '@angular/core/testing';

import { NetworkconnectedGuard } from './networkconnected.guard';

describe('NetworkconnectedGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NetworkconnectedGuard]
    });
  });

  it('should ...', inject([NetworkconnectedGuard], (guard: NetworkconnectedGuard) => {
    expect(guard).toBeTruthy();
  }));
});
