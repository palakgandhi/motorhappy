import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { NetworkService } from '../services/network.service';

@Injectable({
  providedIn: 'root'
})
export class NetworkconnectedGuard implements CanActivate {

  constructor(public network: NetworkService, public router: Router) { }

  canActivate() : boolean {

    if(!this.network.checkConnection()) {
      return true;
    } else {
      return false;
    }
  }
}