import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountverificationPage } from './accountverification.page';

describe('AccountverificationPage', () => {
  let component: AccountverificationPage;
  let fixture: ComponentFixture<AccountverificationPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountverificationPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountverificationPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
