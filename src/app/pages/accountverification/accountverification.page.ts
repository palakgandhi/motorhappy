import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-accountverification',
  templateUrl: './accountverification.page.html',
  styleUrls: ['./accountverification.page.scss'],
})
export class AccountverificationPage implements OnInit {

  isVerified;
  identificationNo: any;
  passportNo: any;
  otp: any;
  cartLogin;

  constructor(private activatedRoute: ActivatedRoute, private router: Router, private userSvc: UserService) { }

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe(queryParams => {
      this.isVerified = queryParams.isVerified;
      if (queryParams.identificationNo)
        this.identificationNo = queryParams.identificationNo;
      else if (queryParams.passportNo)
        this.passportNo = queryParams.passportNo;
      if (queryParams.cars)
      this.cartLogin = queryParams.cars;
    });

    if (this.isVerified === 'false') {
      this.resendOTP();
    }
  }

  resendOTP() {
    let payload = { };
    if (this.identificationNo)
      payload['identificationNo'] = this.identificationNo;

    if (this.passportNo)
      payload['passportNo'] = this.passportNo;

    this.userSvc.resendVerificationRegister(payload).subscribe(resp => {
      if (resp != null)
        return;
    });
  }

  submit() {
    if(!this.otp || (!this.identificationNo && !this.passportNo))
      return;

    let payload = { oneTimePassword: this.otp };
    if (this.identificationNo)
      payload['identificationNo'] = this.identificationNo;

    if (this.passportNo)
      payload['passportNo'] = this.passportNo;

    this.userSvc.verifyRegister(payload).subscribe(resp => {
      if (!resp.authorisationToken)
        return;

      localStorage.setItem('token', resp.authorisationToken);

      if(this.cartLogin){
        this.router.navigate(['/cartpersonaldetail']);
        return;
      }
      this.router.navigate(['/home']);
    });
  }
  
}