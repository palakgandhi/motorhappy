import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { ToastController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-documents',
  templateUrl: './documents.page.html',
  styleUrls: ['./documents.page.scss'],
})
export class DocumentsPage implements OnInit {

  token = localStorage.getItem('token');
  vehiclesDetails : any;
  selectedPolicy = null;
  selectedDocument : any;
  policyId: any;
  policies = [];
  mailTo: any = {name: '', email: ''};
  url;

  constructor(private activatedRoute: ActivatedRoute, private userSvc: UserService, public toastController: ToastController) { }

  ngOnInit() {
    if(!this.token){
      return
    }

    this.activatedRoute.queryParams.subscribe(queryParams => {
      if(!queryParams.policyNumber)
        return;
        this.policyId = queryParams.policyNumber;
    });

    this.userSvc.getVehicles({authorisationToken:this.token,includePolicies:'true'}).subscribe(resp => {
      this.vehiclesDetails = resp;
      this.vehiclesDetails.map(vehicle => this.policies = this.policies.concat(vehicle.CustomerOffers));
      this.policies.map(policy => {
        if (policy.policyNumber != this.policyId)
          return;
  
        this.selectedPolicy = policy;
      });
    });
  }

  onSelectChange() {
    this.selectedDocument = null;
  }
 
  viewDocument() {
    if (!this.selectedDocument)
      return;

    this.url = "https://peqa.liquidcapital.co.za/PinkElephant/Document/NavigateToPolicyDocument?documentationId="+this.selectedDocument.documentationID+"&documentTemplateToken="+this.selectedDocument.documentTemplateToken+"&documentToken="+this.selectedDocument.documentToken;
  }

  sendDocument() {
    if (!this.selectedPolicy || !this.mailTo.name || !this.mailTo.email) {
      return;
    }

    let payload = {
      authorisationToken: this.token,
      customerOfferId: this.selectedPolicy.customerOfferId,
      destinationEmailAddress: this.mailTo.email
    }

    this.userSvc.sendDocument(payload).subscribe(async resp => {
      if (resp != null)
        return;

      const toast = await this.toastController.create({
        message: ' Successfully submitted Email request',
        duration: 3000
      });
      toast.present();
    });
  }

}
