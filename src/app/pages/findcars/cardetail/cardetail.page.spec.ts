import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardetailPage } from './cardetail.page';

describe('CardetailPage', () => {
  let component: CardetailPage;
  let fixture: ComponentFixture<CardetailPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardetailPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardetailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
