import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import * as algoliasearch from 'algoliasearch';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { FormValidators } from '../../../services/formvalidators';
import { UserService } from '../../../services/user.service';

@Component({
  selector: 'app-cardetail',
  templateUrl: './cardetail.page.html',
  styleUrls: ['./cardetail.page.scss'],
})
export class CardetailPage implements OnInit {

  contactForm: FormGroup;
  formSubmitted : boolean = false;
  urlLink;
  selectedCar: any;
  client;

  constructor(private activatedRoute: ActivatedRoute, private fb: FormBuilder, private userSvc: UserService) { 
    this.client = algoliasearch('X88TY7ZVSC', '286876f0caba5312774aeb09f98e6ea6');
    let email = new FormControl('', [Validators.required, Validators.email]);
    this.contactForm = this.fb.group({
      'first_name': [null, Validators.required],
      'last_name': [null, Validators.required],
      'email': email,
      'mobole_no' : [null, [Validators.required, FormValidators.phoneNumber]],
    });
  }

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe(queryParams => {
      this.urlLink = queryParams.HTMLLink;
      let index = this.client.initIndex('live_MHCARS');
      index.search(this.urlLink, (err, content) => {
        this.selectedCar = content.hits[0];  
      });
    });
  }

  // submit(){
  //   this.formSubmitted = true;
  //   if(!this.contactForm.valid){
  //     return
  //   }
  //   let payload = this.contactForm.value;
  //   payload.dealer_code = this.selectedCar.DealerCode;
  //   payload.make = this.selectedCar.MakeName;
  //   payload.model	= this.selectedCar.ModelName;
  //   payload.dealer_name = this.selectedCar.DealerName;
  //   payload.stock_number = this.selectedCar.stockNumber;
  //   payload.year = this.selectedCar.Year;
  //   payload.mm_code = this.selectedCar.MMCode;
  //   payload.car_name = this.selectedCar.FriendlyName;
  //   payload.req_url = this.selectedCar.HTMLLink
  //   console.log(payload);
  //   this.userSvc.contactDealer(payload).subscribe(resp => {
  //     console.log(resp);
  //   });
  // }
}
