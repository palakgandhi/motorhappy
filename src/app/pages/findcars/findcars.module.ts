import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';
import { NgAisModule } from 'angular-instantsearch';
import { AccordionModule } from "ngx-accordion";
import { FindcarsPage } from './findcars.page';

const routes: Routes = [
  {
    path: '',
    component: FindcarsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NgAisModule.forRoot(),
    AccordionModule,
    RouterModule.forChild(routes)
  ],
  declarations: [FindcarsPage]
})
export class FindcarsPageModule {}
