import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FindcarsPage } from './findcars.page';

describe('FindcarsPage', () => {
  let component: FindcarsPage;
  let fixture: ComponentFixture<FindcarsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FindcarsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FindcarsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
