import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-findcars',
  templateUrl: './findcars.page.html',
  styleUrls: ['./findcars.page.scss'],
})
export class FindcarsPage implements OnInit {

  config: any = {
    'apiKey': '286876f0caba5312774aeb09f98e6ea6',
    'appId': 'X88TY7ZVSC',
    'indexName': 'live_MHCARS', 
    'searchParameters': {hitsPerPage: 10}}
  selectedCar: any;
  
  constructor(private router: Router, public loadingController: LoadingController) { }

  ngOnInit() { 
    this.presentLoading() 
  }

  async presentLoading() {
    const loading = await this.loadingController.create({
      spinner: 'dots',
      duration: 5000
    });
    await loading.present();
  }

  viewDetail(HTMLLink){
    this.selectedCar = HTMLLink;
    this.router.navigate(['/cardetail'], { queryParams: {HTMLLink: this.selectedCar}});
  }
}
