import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { UserService } from '../../services/user.service';
import { FormValidators } from '../../services/formvalidators';

@Component({
  selector: 'app-forgotpassword',
  templateUrl: './forgotpassword.page.html',
  styleUrls: ['./forgotpassword.page.scss'],
})
export class ForgotpasswordPage implements OnInit {

  forgotPassword : FormGroup;
  OTPForm : FormGroup;
  userDetail : any;
  formSubmitted : boolean = false;
  public type = 'password';
  public showPass = false;
  isOTP : boolean = false;

  constructor(private fb: FormBuilder, private userSvc: UserService, private navCtrl: NavController) { 
    this.forgotPassword = this.fb.group({
      'type': ['id'],
      'identificationNo': [null, [Validators.required,Validators.minLength(13), Validators.maxLength(13)]],
      'passportNo': [null, []],
      'newPassword' : [null, [Validators.required, FormValidators.password]]
    });
    this.OTPForm = this.fb.group({
      'oneTimePassword': [null, Validators.required]
    });
  }
  
  ngOnInit() { }

  showPassword() {
    this.showPass = !this.showPass;
 
    if(this.showPass){
      this.type = 'text';
    } else {
      this.type = 'password';
    }
  }

  formChange(){
    this.formSubmitted = false;
  }

  onTypeChangePassword(event) {
    if (event == 'id') {
      this.forgotPassword.get('identificationNo').setValidators([Validators.required]);
      this.forgotPassword.get('passportNo').setValidators(null);
      this.forgotPassword.get('passportNo').setValue(null);
    } else {
      this.forgotPassword.get('passportNo').setValidators([Validators.required]);
      this.forgotPassword.get('identificationNo').setValidators(null);
      this.forgotPassword.get('identificationNo').setValue(null);
    }

    this.forgotPassword.updateValueAndValidity();
  }

  validFormData(payload){
    if(payload.type == 'id') {
      delete payload.passportNo;
    }
    if(payload.type == 'passport') {
      delete payload.identificationNo;
    }
    delete payload.type;
  }

  updatePassword(){
    this.formSubmitted = true;
    if(!this.forgotPassword.valid){
      return;
    }
    let payload = this.forgotPassword.value;
    this.validFormData(payload);
    this.userSvc.forgotPassword(payload).subscribe(resp => {
      if(resp == null){
        this.isOTP = true;
      }
    });
  }

  confirmOTP(){
    if(!this.OTPForm.valid){
      return;
    }
    let form1 = this.forgotPassword.value;
    let form2 = this.OTPForm.value;
    let payload = Object.assign(form1 , form2);
    this.validFormData(payload);
    this.userSvc.resetPasswordverification(payload).subscribe(resp => {
      if(resp.authorisationToken){
        this.userDetail = resp;
        localStorage.setItem('token', this.userDetail.authorisationToken);
        this.navCtrl.navigateRoot(['/home']);
      }
    });
  }

}
