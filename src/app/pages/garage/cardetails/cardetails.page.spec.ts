import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardetailsPage } from './cardetails.page';

describe('CardetailsPage', () => {
  let component: CardetailsPage;
  let fixture: ComponentFixture<CardetailsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardetailsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardetailsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
