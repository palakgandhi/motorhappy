import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cardetails',
  templateUrl: './cardetails.page.html',
  styleUrls: ['./cardetails.page.scss'],
})
export class CardetailsPage implements OnInit {

  selectedVehicle = localStorage.getItem('selectedVehicle');
  carDetails = JSON.parse(this.selectedVehicle);
  
  constructor() { }

  ngOnInit() { }

}
