import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { MygaragePage } from './mygarage.page';
import { SharedModule } from '../../../shared/shared.module';
import { AccordionModule } from "ngx-accordion";

const routes: Routes = [
  {
    path: '',
    component: MygaragePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedModule,
    AccordionModule,
    RouterModule.forChild(routes)
  ],
  declarations: [MygaragePage]
})
export class MygaragePageModule {}
