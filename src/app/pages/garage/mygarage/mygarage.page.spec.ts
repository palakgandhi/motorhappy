import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MygaragePage } from './mygarage.page';

describe('MygaragePage', () => {
  let component: MygaragePage;
  let fixture: ComponentFixture<MygaragePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MygaragePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MygaragePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
