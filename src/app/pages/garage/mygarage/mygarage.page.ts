import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../../../services/user.service';

@Component({
  selector: 'app-mygarage',
  templateUrl: './mygarage.page.html',
  styleUrls: ['./mygarage.page.scss'],
})
export class MygaragePage implements OnInit {

  token = localStorage.getItem('token');
  vehiclesDetails : any = [];
  selectedVehicle: any;

  constructor(private userSvc: UserService, private router: Router) { }

  ngOnInit() {
    if(!this.token) {
      return;
    }
    this.userSvc.getVehicles({authorisationToken:this.token,includePolicies:'true'}).subscribe(resp => {
      this.vehiclesDetails = resp;
    });
  }

  navigateToProfile(vehicle) {
    this.selectedVehicle = vehicle;
    localStorage.setItem('selectedVehicle', JSON.stringify(this.selectedVehicle));
    this.router.navigate(['/cardetails']);
  }

  showDetails(plan) {
    this.selectedVehicle = plan;
    localStorage.setItem('selectedVehicle', JSON.stringify(this.selectedVehicle));
    this.router.navigate(['/viewplan']);
  }

  getDocument(policyNumber) {
    var policyNo = policyNumber;
    this.router.navigate(['/documents'], { queryParams: {policyNumber: policyNo}});
  }
}
