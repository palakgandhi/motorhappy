import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ViewplanPage } from './viewplan.page';
import { SharedModule } from '../../../shared/shared.module';
import { AccordionModule } from "ngx-accordion";

const routes: Routes = [
  {
    path: '',
    component: ViewplanPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedModule,
    AccordionModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ViewplanPage]
})
export class ViewplanPageModule {}
