import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewplanPage } from './viewplan.page';

describe('ViewplanPage', () => {
  let component: ViewplanPage;
  let fixture: ComponentFixture<ViewplanPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewplanPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewplanPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
