import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-viewplan',
  templateUrl: './viewplan.page.html',
  styleUrls: ['./viewplan.page.scss'],
})
export class ViewplanPage implements OnInit {

  selectedVehicle = localStorage.getItem('selectedVehicle');
  vehicleDetails = JSON.parse(this.selectedVehicle);

  constructor(private router: Router) { }

  ngOnInit() { }

  getDocument(policyNumber){
    var policyNo = policyNumber;
    this.router.navigate(['/documents'], { queryParams: {policyNumber: policyNo}});
  }

}
