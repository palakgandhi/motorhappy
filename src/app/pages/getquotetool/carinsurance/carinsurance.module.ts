import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CarinsurancePage } from './carinsurance.page';
import { AccordionModule } from "ngx-accordion";
import { SharedModule } from '../../../shared/shared.module';
import { AutocompleteModule } from 'ng2-input-autocomplete';

const routes: Routes = [
  {
    path: '',
    component: CarinsurancePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    AccordionModule,
    SharedModule,
    AutocompleteModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CarinsurancePage]
})
export class CarinsurancePageModule {}
