import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarinsurancePage } from './carinsurance.page';

describe('CarinsurancePage', () => {
  let component: CarinsurancePage;
  let fixture: ComponentFixture<CarinsurancePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarinsurancePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarinsurancePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
