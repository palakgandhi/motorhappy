import { Component, OnInit, ViewChild } from '@angular/core';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { FormValidators } from '../../../services/formvalidators';
import { UserService } from '../../../services/user.service';
import { HttpParams } from '@angular/common/http';
import { Content } from '@ionic/angular';
import { Device } from '@ionic-native/device/ngx';

@Component({
  selector: 'app-carinsurance',
  templateUrl: './carinsurance.page.html',
  styleUrls: ['./carinsurance.page.scss'],
})
export class CarinsurancePage implements OnInit {
  @ViewChild(Content) content: Content;

  config: any = {'placeholder': '', 'sourceField': 'VehicleMake'}
  config2: any = {'placeholder': '', 'sourceField': 'VehicleModel'};
  insuranceForm: FormGroup;
  formSubmitted : boolean = false;
  vehicleMake;
  registerDate: any;
  Model: any;
  callDetail: Boolean = false;

  constructor(private fb: FormBuilder, private userSvc: UserService,private device: Device) { 
    let email = new FormControl('', [Validators.required, Validators.email]);
    this.insuranceForm = this.fb.group({
      'leadFirstName': [null, Validators.required],
      'leadEmailId': email,
      'leadContactNumber' : [null, [Validators.required, FormValidators.phoneNumber]],
      'leadYear': [null, Validators.required],
      'leadMake' : [null, Validators.required],
      'leadMakeId': [null],
      'leadModel': [null, Validators.required],
      'leadModelId': [null],
    });
  }

  maxDateString() {
    return new Date().toISOString().split('T')[0];
  }

  logScrolling() {
    this.content.scrollByPoint(0,200,200);
  }

  ngOnInit() {
    this.insuranceForm.controls['leadMake'].disable();
    this.insuranceForm.controls['leadModel'].disable();
   }

  vehicleRegisterYear(event){
    this.insuranceForm.patchValue({
      MHMake: null,
      MHModel: null, 
    });
    this.registerDate = event.concat('-01-01');
    this.userSvc.getVehicleMakeList().subscribe(resp => {
      this.vehicleMake = resp;
    });
    this.insuranceForm.controls['leadMake'].enable();
  }

  onSelectMake(item:any){
    this.insuranceForm.patchValue({
      MHModel: null,
    });
    if(!item.VehicleMakeId){
      return;
    }
    this.insuranceForm.get('leadMake').setValue(item.VehicleMake)
    this.insuranceForm.get('leadMakeId').setValue(item.VehicleMakeId);
    this.userSvc.getVehicleModelList(new HttpParams().set('dateOfFirstRegistration',this.registerDate)
    .set('vehicleMakeId',this.insuranceForm.controls['leadMakeId'].value)).subscribe(resp => {
      this.Model = resp;
    });
    this.insuranceForm.controls['leadModel'].enable();
  }
  
  onSelectModel(item:any){
    if(!item.VehicleMakeId){
      return;
    }
    this.insuranceForm.get('leadModel').setValue(item.VehicleModel);
    this.insuranceForm.get('leadModelId').setValue(item.VehicleModelId);
  }

  submit(){
    this.formSubmitted = true;
    if(!this.insuranceForm.valid){
      return;
    }
    let payload = this.insuranceForm.value;
    payload.utm_source = 'Insurance_MHWEB';
    payload.utm_medium = 'Organic or Direct';
    payload.utm_term = 'Car Insurance';
    payload.utm_campaign = 'car-insurance';
    this.userSvc.CreateLead(payload).subscribe(resp => {
      if(!resp.leadId){
        return;
      }
      this.userSvc.LeadSubmission({authorisationToken: null,leadId: resp.leadId}).subscribe(resp => {
        if(!resp.leadId){
          return;
        }
        payload.leadType = 'Car Insurance'
        payload.leadId = resp.leadId;
        payload.device_platform = this.device.platform;
        payload.device_model = this.device.model;
        payload.device_version = this.device.version;
        this.userSvc.lead(payload).subscribe(resp => {
          if(resp.status == 'false'){
            return;
          }
          this.callDetail = true;
        });
      });
    });
  }

  backtoQuote(){
    this.callDetail = false;
  }
}
