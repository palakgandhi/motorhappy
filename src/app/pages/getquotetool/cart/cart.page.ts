import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../services/user.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.page.html',
  styleUrls: ['./cart.page.scss'],
})
export class CartPage implements OnInit {

  quotePlan;
  PlanTypeId;
  cartDetails = [];
  totalCost;
  planselected : boolean = false;
  selectTermCondi : boolean = false;

  constructor(
    private userSvc: UserService, 
    private activatedRoute: ActivatedRoute, 
    private router: Router,
    private toastCtrl: ToastController ) { }

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe(queryParams => {
      this.quotePlan = queryParams;
      this.PlanTypeId = queryParams.planID;
      localStorage.setItem('QuotePlanType', JSON.stringify(this.quotePlan));
    });
    
    this.getLocalCartDetails();
  }

  getLocalCartDetails() {
    if(localStorage.getItem('cartDetails')) {
      this.cartDetails = JSON.parse(localStorage.getItem('cartDetails'));
      this.totalCost = 0;
      this.cartDetails.map(item => {
        if(item.monthly == true || item.yearly == true) 
          this.planselected = true;
        if(item.termcondition == true)
          this.selectTermCondi = true;
        if(item.monthly == true) 
          this.totalCost += item.MonthlyTotalCostVAT
        else if(item.yearly == true) 
          this.totalCost += item.TotalCostVAT
      });  
      return;
    } else if (localStorage.getItem('GuestUser')){
      this.leadBasketGet();
      return; 
    } else if (localStorage.getItem('customerActionId')) {
      this.basketGet();
    }
  }

  leadBasketGet() {
    this.userSvc.leadBasketGet({authorisationToken:'', leadId: localStorage.getItem('GuestUser')})
    .subscribe(resp => {
      this.cartDetails = resp;
      localStorage.setItem('cartDetails', JSON.stringify(this.cartDetails));
      this.totalCost = '0.00'
    });
  }

  basketGet(){
    this.userSvc.basketGet({authorisationToken: localStorage.getItem('token'), customerActionId: localStorage.getItem('customerActionId')})
    .subscribe(resp => {
      this.cartDetails =resp;
      localStorage.setItem('cartDetails', JSON.stringify(this.cartDetails));
      this.totalCost = '0.00'
    });
  }

  removeCart(quoteId) {
    if(localStorage.getItem('token')){
      let payload = {
        authorisationToken: localStorage.getItem('token'),
        quoteId: quoteId,
        customerActionId: localStorage.getItem('customerActionId')
      };
      this.userSvc.basketQuoteRemove(payload).subscribe(resp => {
        if(resp){
          return;
        }
        this.planselected = false;
        this.selectTermCondi = false;
        this.basketGet();
      });
      return;
    }
    this.removeCartGuest(quoteId);
  }

  removeCartGuest(quoteId) {
    let payload = {
      quoteId: quoteId,
      leadId: localStorage.getItem('GuestUser')
    }
    this.userSvc.leadBasketQuoteRemove(payload).subscribe(resp => {
      if(resp){
        return;
      }
      this.planselected = false;
      this.selectTermCondi = false;
      this.leadBasketGet();
    });
  }

  quotePaymentMonthly() {
    this.planselected = true;
    let total = 0;
    this.cartDetails.map(item => { 
      item.monthly = true ; 
      item.yearly = false; 
      total += item.MonthlyTotalCostVAT;
      this.totalCost = total.toFixed(2);
      localStorage.setItem('totalCost', this.totalCost);
    });
  }

  quotePaymentYearly() {
    this.planselected = true;
    let total = 0;
    this.cartDetails.map(item => { 
      if(item.PinkBoxPlanTypeId != 'df99ec60-c075-4a60-a3f1-97531f84bb47') {
        item.yearly = true ; 
      }
      item.monthly = false;
      total += item.TotalCostVAT;
      this.totalCost = total.toFixed(2);
      localStorage.setItem('totalCost', this.totalCost);
    });
  }

  quoteTermCondi() {
    var termcondition = this.cartDetails.filter(item => { return item.termcondition == true });
    if(this.cartDetails.length != termcondition.length){
      this.selectTermCondi = false;
      return;
    }
    this.selectTermCondi = true;
  }

  async presentToast() {
    const toast = await this.toastCtrl.create({
      message: 'Kindly select the same payment option for all products.',
      duration: 2000
    });
    toast.present();
  }

  submit() {
    let totalItems = this.cartDetails.length;
    let monthly = this.cartDetails.filter(item => item.monthly == true).length;
    let yearly = this.cartDetails.filter(item => item.yearly == true).length;

    if (totalItems != monthly && totalItems != yearly) {
      this.presentToast();
      return;
    }
    localStorage.setItem('cartDetails', JSON.stringify(this.cartDetails));
    if(localStorage.getItem('token')){
      this.router.navigate(['/cartpersonaldetail']);
      return;
    }
    this.router.navigate(['/'], {queryParams : {cart: 'true'}});
  }

}
