import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CartpaymentconfirmationPage } from './cartpaymentconfirmation.page';

const routes: Routes = [
  {
    path: '',
    component: CartpaymentconfirmationPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CartpaymentconfirmationPage]
})
export class CartpaymentconfirmationPageModule {}
