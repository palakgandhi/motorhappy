import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../services/user.service';

@Component({
  selector: 'app-cartpaymentconfirmation',
  templateUrl: './cartpaymentconfirmation.page.html',
  styleUrls: ['./cartpaymentconfirmation.page.scss'],
})
export class CartpaymentconfirmationPage implements OnInit {

  quoteplan;
  cartDetails;
  customerId;
  quoteFormValue;
  physicalDetails;
  contactDetails;
  personalDetails;
  bankDetails : number;
  paymentMethod; 
  accountType;
  confirmForm: any = {registrationNo: '', termCondition: ''};
  token = localStorage.getItem('token');
  total = localStorage.getItem('totalCost');
  externalEFTreference;
  customerActionId;
  customerDetails;
  showBankDetails : boolean = false;

  constructor(private userSvc: UserService) { }

  ngOnInit() {
    this.quoteplan = JSON.parse(localStorage.getItem('QuotePlanType'));
    
    this.cartDetails = JSON.parse(localStorage.getItem('cartDetails'));

    this.quoteFormValue = JSON.parse(localStorage.getItem('QuoteFormValue'));
    
    if(this.quoteFormValue) {
      this.confirmForm.registrationNo = this.quoteFormValue.MHRegistrationNo;
    }

    this.userSvc.getCustomerProfile({authorisationToken: this.token}).subscribe(resp => {
      if(!resp.customerId) {
        return;
      }

      this.customerId = resp.customerId;
      this.physicalDetails = resp.postalAddress;
      this.contactDetails = resp.contactDetails;
      this.personalDetails = resp.personalDetails;
      if(!resp.bankDetails.paymentMethod) {
        return;
      }

      this.bankDetails = resp.bankDetails.paymentMethod;
      this.userSvc.getPaymentMethod().subscribe(resp => {
        let method = resp;
        this.paymentMethod = method.filter(x => { return x.Key == this.bankDetails});
      });
    });
  }

  submit() {
    if (!this.confirmForm.registrationNo || !this.confirmForm.termCondition) {
      return;
    }

    var quoteIds = this.cartDetails.map(item => { return item.ID});
    let payloadREG = {
      authorisationToken: this.token,
      vehicleRegistrationNo: this.confirmForm.registrationNo,
      quoteIds: quoteIds
    }
    this.userSvc.quoteUpdateWithVehicleRegistrationNo(payloadREG).subscribe(resp => {
      if(resp) {
        return;
      }

      this.quoteFormValue.MHRegistrationNo = this.confirmForm.registrationNo
      localStorage.setItem('QuoteFormValue', JSON.stringify(this.quoteFormValue));
      let payload = {};
      payload['authorisationToken'] = this.token;
      payload['customerId'] = this.customerId;
      payload['customerActionId'] = localStorage.getItem('customerActionId');
      payload['actionTypeId'] = 1;
      payload['acceptTsAndCs'] = this.confirmForm.termCondition;
      this.userSvc.customerActionUpdate(payload).subscribe(resp => {
        if(!resp.customerActionId) {
          return;
        }  
        this.customerActionId = resp.customerActionId;
        this.externalEFTreference = resp.externalEFTreference;
        this.paymentMethod.map(item => {
          if(item.Value == 'Credit Card') {
            return;
          } else {
            this.customerActionUpdateSubmit(payload);
          }
        });
      });
    });
  }

  customerActionUpdateSubmit(payload) {
    this.userSvc.customerActionSubmit(payload).subscribe(resp => {
      if(!resp.customerActionId) {
        return;
      }
      
      this.customerDetails = resp;
      this.userSvc.getBankAccountType().subscribe(resp => {
        this.accountType = resp.filter(item => { return item.Key == this.customerDetails.bankDetails.bankAccountType });
      });
      this.showBankDetails = true;
    }); 
  }

}
