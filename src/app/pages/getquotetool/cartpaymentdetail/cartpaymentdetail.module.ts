import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';
import { SharedModule } from '../../../shared/shared.module';
import { CartpaymentdetailPage } from './cartpaymentdetail.page';
import { AutocompleteModule } from 'ng2-input-autocomplete';

const routes: Routes = [
  {
    path: '',
    component: CartpaymentdetailPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    SharedModule,
    AutocompleteModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CartpaymentdetailPage]
})
export class CartpaymentdetailPageModule {}
