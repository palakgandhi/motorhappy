import { Component, OnInit, ViewChild } from '@angular/core';
import { UserService } from '../../../services/user.service';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http'; 
import { Content } from '@ionic/angular';

@Component({
  selector: 'app-cartpaymentdetail',
  templateUrl: './cartpaymentdetail.page.html',
  styleUrls: ['./cartpaymentdetail.page.scss'],
})
export class CartpaymentdetailPage implements OnInit {
  @ViewChild(Content) content: Content;

  configBranch: any = {'placeholder': '', 'sourceField': 'branchCode'}
  debitOrderForm: FormGroup;
  formSubmitted: boolean = false;
  token = localStorage.getItem('token');
  paymentForm: any = {PaymentMethod: '', termCondition: ''};
  quoteplan;
  cartDetails = [];
  paymentMethod;
  customerId;
  DebitOrder : boolean = false;
  bankDetails;
  accountType;
  groupByName = {};
  bank;
  branch = [];
 
  constructor(
    private userSvc: UserService, 
    private router: Router, 
    private fb: FormBuilder, 
    private http : HttpClient) {
      this.debitOrderForm = this.fb.group({
        'bankAccountHolder': [null, Validators.required],
        'bankName': [null, Validators.required],
        'bankDetails': [null, Validators.required],
        'bankAccountType': [null, Validators.required],
        'bankAccountNumber': [null, Validators.required],
        'debitOrderDay': [null, Validators.required]
      });
  }

  ngOnInit() {
    this.debitOrderForm.controls['bankDetails'].disable();

    this.quoteplan = JSON.parse(localStorage.getItem('QuotePlanType'));
    
    this.cartDetails = JSON.parse(localStorage.getItem('cartDetails'));

    this.userSvc.getPaymentMethod().subscribe(resp => {
      this.paymentMethod = resp;
      this.cartDetails.map(item => {
        if (item.monthly == true) {
          var index = this.paymentMethod.filter(x => { return x.Value == "Debit Order"});
          this.paymentMethod = index;
        }
        else if (item.yearly == true) {
          var index = this.paymentMethod.findIndex(x => { return x.Value == "Debit Order"});
          if(index > -1) {
            this.paymentMethod.splice(index , 1);
          }
        }
      });
    });

    this.userSvc.getCustomerProfile({authorisationToken:this.token}).subscribe(resp => {
      if(!resp.customerId) {
        return;
      }
      this.customerId = resp.customerId;
      this.paymentForm.PaymentMethod = resp.bankDetails.paymentMethod;
      
    });

    this.debitOrder();
  }

  submit() {
    if (!this.paymentForm.PaymentMethod || !this.paymentForm.termCondition) {
      return;
    }
    let payload = {};
    payload['authorisationToken'] = this.token;
    payload['customerId'] = this.customerId;
    payload['actionTypeId'] = 1;
    payload['bankDetails'] = {
      paymentMethod : this.paymentForm.PaymentMethod
    }
    this.userSvc.customerActionUpdate(payload).subscribe(resp => {
      if(!resp.customerId) {
        return;
      }
      this.router.navigate(['/cartpaymentconfirmation']);
    });
  }

  logScrolling() {
    this.content.scrollByPoint(0,100,200);
  }

  debitOrder() {
    this.cartDetails.map(item => {
      if (item.monthly == true) {
        this.DebitOrder = true;
        this.http.get('assets/bankdetails/bankdetails.json').subscribe(resp => {
          this.bankDetails = resp;
          this.bankDetails.map(item =>  {
            let bank = item.Name.split(",");
            let bankname = bank[1];
            this.groupByName [bankname] = this.groupByName [bankname] || [];
            this.groupByName [bankname].push({ branchCode: item.Name, branchId: item.ID });
          });
          this.bank = this.groupByName;
          // this.bankName = data.filter((v, i, a) => a.indexOf(v) === i);
        });

        this.userSvc.getBankAccountType().subscribe(resp => {
          this.accountType = resp;
        });
      }
    });
  }
  
  selectBranch(event) {
    this.debitOrderForm.controls['bankDetails'].enable();
    this.debitOrderForm.patchValue({
      bankDetails: null
    });
    this.branch = this.bank[event];
  }

  onSelectBranchCode(item) {
    if(!item.branchCode) {
      return;
    }
    this.debitOrderForm.get('bankDetails').setValue(item.branchId);
  }

  debitOrderSubmit() {
    this.formSubmitted = true;
    if(!this.debitOrderForm.valid) {
      return;
    }

    let payload = {} ;
    payload['authorisationToken'] = this.token;
    payload['customerActionId'] = localStorage.getItem('customerActionId');
    payload['actionTypeId'] = 1;
    payload['customerId'] = this.customerId;
    payload['paymentReceived'] = true;
    payload['allowEditing'] = true;
    payload['acceptTsAndCs'] = this.paymentForm.termCondition;
    payload['bankDetails'] = this.debitOrderForm.value;
    payload['bankDetails'].paymentMethod = this.paymentForm.PaymentMethod;
    this.userSvc.customerActionUpdate(payload).subscribe(resp => {
      if(!resp.customerActionId) {
        return;
      }
      
      this.router.navigate(['/cartpaymentconfirmation']);
    });
  }

}
