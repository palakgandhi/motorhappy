import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CartpersonaldetailPage } from './cartpersonaldetail.page';

describe('CartpersonaldetailPage', () => {
  let component: CartpersonaldetailPage;
  let fixture: ComponentFixture<CartpersonaldetailPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CartpersonaldetailPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CartpersonaldetailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
