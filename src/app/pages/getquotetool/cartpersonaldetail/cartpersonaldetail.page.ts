import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../services/user.service';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-cartpersonaldetail',
  templateUrl: './cartpersonaldetail.page.html',
  styleUrls: ['./cartpersonaldetail.page.scss'],
})
export class CartpersonaldetailPage implements OnInit {

  postalAddressForm : FormGroup;
  postalAddress : any = {};
  formSubmitted : boolean = false;
  quoteFormValue; 
  quoteplan;
  cartDetails;
  customerId;
  suburbList: any;
  postCodeDesc : any = [];
  token = localStorage.getItem('token');
 
  constructor(private userSvc: UserService, private fb: FormBuilder, private router: Router) { 
    this.postalAddressForm = this.fb.group({
      'addressLine1': [null, Validators.required],
      'addressLine2': [null, Validators.required],
      'addressLine3': [null, Validators.required],
      'Suburb': [null, Validators.required]
    });
  }

  ngOnInit() {
    this.quoteFormValue = JSON.parse(localStorage.getItem('QuoteFormValue'));
    this.quoteplan = JSON.parse(localStorage.getItem('QuotePlanType'));

    this.cartDetails = JSON.parse(localStorage.getItem('cartDetails'));

    this.userSvc.getCustomerProfile({authorisationToken:this.token}).subscribe(resp => {
      if(!resp.customerId){
        return;
      }
      this.customerId = resp.customerId;
      this.postalAddress = resp.postalAddress;
      if(this.postalAddress.postCodeDesc != '') {
        this.userSvc.getSingleSuburb(this.postalAddress.postCodeDesc).subscribe(resp => {
           this.suburbList = resp.value;
           this.postalAddress.Suburb = this.suburbList;
           this.postalAddressForm.patchValue(this.postalAddress);
       });
      }
    });
  }

  getPostCodeData(event) {
    let Suburb = event;
    this.userSvc.getSuburbList(Suburb).subscribe(resp => {
      this.postCodeDesc = resp.value;
      this.postalAddressForm.patchValue(this.postCodeDesc);
    });
  }

  submit() {
    this.formSubmitted = true;
    if(!this.postalAddressForm.valid) {
      return;
    }
    let payload = {};
    payload['authorisationToken'] = this.token;
    payload['customerId'] = this.customerId;
    payload['actionTypeId'] = 1;
    this.userSvc.customerActionUpdate(payload).subscribe(resp => {
      if(!resp.customerActionId) {
        return;
      }
      if(!localStorage.getItem('customerActionId')) {
        let payload1 = {};
        payload1['authorisationToken'] = this.token;
        payload1['customerActionId'] = resp.customerActionId;
        payload1['leadId'] = localStorage.getItem('GuestUser');
        this.userSvc.copyLeadToCustomerAction(payload1).subscribe(resp => {
          if(!resp.customerActionId) {
            return;
          }
          localStorage.setItem('customerActionId', resp.customerActionId);
          localStorage.removeItem('GuestUser');
        });
      }
      let payload2 = {};
      payload2['authorisationToken'] = this.token;
      payload2['customerActionId'] = resp.customerActionId;
      payload2['postalAddress'] = this.postalAddressForm.value;      
      payload2['postalAddress'].postCode = this.postalAddressForm.value.Suburb[0].ID;
      payload2['postalAddress'].provinceId = this.postalAddressForm.value.Suburb[0].Code;
      delete payload2['postalAddress'].Suburb;
      this.userSvc.customerUpdate(payload2).subscribe(() => {
        this.router.navigate(['/cartpaymentdetail']);
      });
    });
  }

}
