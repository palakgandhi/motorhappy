import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GetquotePage } from './getquote.page';

describe('GetquotePage', () => {
  let component: GetquotePage;
  let fixture: ComponentFixture<GetquotePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GetquotePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GetquotePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
