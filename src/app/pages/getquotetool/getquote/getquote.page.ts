import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../services/user.service';

@Component({
  selector: 'app-getquote',
  templateUrl: './getquote.page.html',
  styleUrls: ['./getquote.page.scss'],
})
export class GetquotePage implements OnInit {

  plan = [];

  constructor(private userSvc: UserService) { }

  ngOnInit() { 
    this.userSvc.getPlanTypes().subscribe(resp => {
      this.plan = resp;
    });

    let keysToRemove = ["MassQuoteResult", "QuoteFormValue", "QuotePlanType", "GuestUser" , "customerActionId", "cartDetails", "totalCost"];

    keysToRemove.forEach(k =>
      localStorage.removeItem(k));
  }

}
