import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';
import { AccordionModule } from "ngx-accordion";
import { QuoteresultPage } from './quoteresult.page';

const routes: Routes = [
  {
    path: '',
    component: QuoteresultPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AccordionModule,
    RouterModule.forChild(routes)
  ],
  declarations: [QuoteresultPage]
})
export class QuoteresultPageModule {}
