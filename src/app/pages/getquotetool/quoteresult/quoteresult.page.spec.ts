import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuoteresultPage } from './quoteresult.page';

describe('QuoteresultPage', () => {
  let component: QuoteresultPage;
  let fixture: ComponentFixture<QuoteresultPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuoteresultPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuoteresultPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
