import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../services/user.service';
import { HttpParams } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { AlertController, ToastController } from '@ionic/angular';

@Component({
  selector: 'app-quoteresult',
  templateUrl: './quoteresult.page.html',
  styleUrls: ['./quoteresult.page.scss'],
})
export class QuoteresultPage implements OnInit {

  quoteFormValue;
  quotePlan;
  PlanTypeId;
  massQuote = [];
  plan = [];
  yersToCover = [];
  coverYears;
  yearsToCoverData = [];
  error = null;
  productMilegeValue;
  productMilege = [];
  cartDetails = [];
    
  constructor(
    private userSvc: UserService,
    private activatedRoute: ActivatedRoute,
    private alertController: AlertController,
    private toastController: ToastController,
    private router: Router) { }

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe(queryParams => {
      this.quotePlan = queryParams;
      this.PlanTypeId = queryParams.planID;
    });

    this.userSvc.getPlanTypes().subscribe(resp => {
      this.plan = resp;
    });

    this.massQuote = JSON.parse(localStorage.getItem('MassQuoteResult'));
    this.quoteFormValue = JSON.parse(localStorage.getItem('QuoteFormValue'));
    this.ProductYearCover();

    if(!localStorage.getItem('token')) {
      this.getLeadBasket();
      return;
    } else {
      this.basketGet();
    }
  }

  ProductYearCover() {
    var selectedPlan = this.massQuote.filter(item => { return item.PinkBoxPlanTypeId == this.PlanTypeId });
    if(selectedPlan.length == 0){
      if(this.PlanTypeId === 'acaadfe3-0156-422b-9b93-f7778fe86925')
      {
        this.error = "Oops! Looks like your car doesn’t qualify for a maintenance plan.To qualify for a maintenance plan, your car cannot be older than 4 years and should have less than 80 000km on the clock!"
      } else {
        this.error = "Unfortunately we do not have a quote option for the car selected. Would you like a quote for another vehicle? Or have a consultant call you back?" 
      }
    } else {
      this.error = null;
    }
    this.userSvc.getProductYearCover(new HttpParams()
    .set('planType',this.PlanTypeId)
    .set('startKm', this.quoteFormValue.MHStartKilometers)
    .set('vehicleModelId', this.quoteFormValue.MHModelID)).subscribe(resp => {
      this.yersToCover = resp;
      this.coverYears = this.yersToCover[0].Value;
      this.ProductMileage();
    });
  }

  ProductMileage() {
    this.yearsToCoverData = this.massQuote.filter(item => { return item.AmountOfYearsToCover == this.coverYears});
    this.userSvc.getProductMileage(new HttpParams()
    .set('amountOfYearsToCover', this.coverYears)
    .set('planTypeId',this.PlanTypeId)
    .set('vehicleModelId', this.quoteFormValue.MHModelID)).subscribe(resp => {
      this.productMilege = resp;
      this.productMilegeValue = null;
    });
  }

  ProductMileageList() {
    this.yearsToCoverData = this.massQuote;
    this.yearsToCoverData = this.yearsToCoverData.filter(item => { return item.KmCovered == this.productMilegeValue && item.AmountOfYearsToCover == this.coverYears});
  }

  async presentAlert() {
    const alert = await this.alertController.create ({
      subHeader: 'If you change your Car Details you will have to restart your quote process.Your quote result might change.',
      message: 'Would you like to continue to change your Car Details?',
      buttons: [
        {
          text: 'Yes',
          handler: () => {
            let params = {};
            params['planID'] = this.PlanTypeId;
            params['planType'] = this.quotePlan.planType;
            this.router.navigate(['/serviceplan'], {queryParams: params});
          }
        },
        {
          text: 'No',
          role: 'cancel'
        }
      ]
    });
    await alert.present();
  }

  backQuote() {
    let params = {};
    params['planID'] = this.quotePlan.planID;
    params['planType'] = this.quotePlan.planType;
    this.router.navigate(['/serviceplan'], {queryParams: params});
  }

  getLeadBasket() {
    if(localStorage.getItem('GuestUser')) {
      this.userSvc.leadBasketGet({authorisationToken:'', leadId: localStorage.getItem('GuestUser')})
      .subscribe(resp => {
        this.cartDetails = resp;
      });
    }
  }

  basketGet() {
    if(localStorage.getItem('customerActionId')) {
      this.userSvc.basketGet({authorisationToken: localStorage.getItem('token'), customerActionId: localStorage.getItem('customerActionId')})
      .subscribe(resp => {
        this.cartDetails = resp;
      });
    }
  }

  addToCart(quoteId) {
    if(localStorage.getItem('token'))
    {   
      if(!localStorage.getItem('customerActionId')) {
        let token = localStorage.getItem('token');
        this.userSvc.getCustomerProfile({authorisationToken: token}).subscribe(resp => {
          if(!resp.customerId) {
            return;
          }
          let payload = {};
          payload['authorisationToken'] = token;
          payload['customerId'] = resp.customerId;
          payload['actionTypeId'] = 1;
          this.userSvc.customerActionUpdate(payload).subscribe(resp => {
            if(!resp.customerActionId) {
              return;
            }
            localStorage.setItem('customerActionId', resp.customerActionId);
            this.basketQuoteAdd(quoteId);
            return;
          });
        });
      }
      this.basketQuoteAdd(quoteId); 
      return;
    }
    this.createLead(quoteId);
  }

  basketQuoteAdd(Id) {
    if(localStorage.getItem('customerActionId')) {
      let token = localStorage.getItem('token');
      let payload = {
      authorisationToken: token,
      quoteId: Id,
      customerActionId: localStorage.getItem('customerActionId')
      };
      this.userSvc.baskedQuoteAdd(payload).subscribe(resp => {
        if(resp) {
          return;
        }
        delete payload.quoteId;
        this.userSvc.basketGet(payload).subscribe(resp => {
          if(!resp[0]) {
            return;
          }
          this.cartDetails = resp;
          localStorage.setItem('cartDetails', JSON.stringify(this.cartDetails));
          this.presentToast();
        });
      });
    }
  }

  createLead(quoteId) {
    let payload = {
      customerActionId: 0,
      email: 'sagar@mavenagency.co.za',
      leadReason: 0,
      name : 'Sagar',
      pinkBoxVehicleModelId : this.quoteFormValue.MHModelID,
      quoteId : 0,
      surname : 'Prajapati',
      telephone : '917405360981',
      year : 0
    }
    if(!localStorage.getItem('GuestUser')) {
      this.userSvc.CreateLead(payload).subscribe(resp => {
        if(!resp.leadId){
          return;
        }
        localStorage.setItem('GuestUser', resp.leadId);
        this.addGetLead(quoteId);
        return;
      });
    }
    this.addGetLead(quoteId);
  }

  addGetLead(Id) {
    if(localStorage.getItem('GuestUser')) {
      let payload = {
        authorisationToken: '',
        quoteId : Id,
        leadId : localStorage.getItem('GuestUser')
      }
      this.userSvc.leadBasketQuoteAdd(payload).subscribe(resp => {
        if(resp){
          return;
        }
        delete payload.quoteId;
        this.userSvc.leadBasketGet(payload).subscribe(resp => {
          if(!resp[0]){
            return;
          }
          this.cartDetails = resp;
          localStorage.setItem('cartDetails', JSON.stringify(this.cartDetails));
          this.presentToast();
        });
      });
    }
  }

  async presentToast() {
    const toast = await this.toastController.create ({
      message: 'Sucessfully Added To Cart',
      duration: 2000
    });
    toast.present();
  }
  
}