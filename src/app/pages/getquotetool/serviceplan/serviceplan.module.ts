import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ServiceplanPage } from './serviceplan.page';
import { SharedModule } from '../../../shared/shared.module'
import { AutocompleteModule } from 'ng2-input-autocomplete';

const routes: Routes = [
  {
    path: '',
    component: ServiceplanPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    SharedModule,
    AutocompleteModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ServiceplanPage]
})
export class ServiceplanPageModule {}
