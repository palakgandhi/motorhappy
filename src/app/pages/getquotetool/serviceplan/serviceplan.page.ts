import { Component, OnInit, ViewChild } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { UserService } from '../../../services/user.service';
import { HttpParams } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { Content } from '@ionic/angular';

@Component({
  selector: 'app-serviceplan',
  templateUrl: './serviceplan.page.html',
  styleUrls: ['./serviceplan.page.scss'],
})

export class ServiceplanPage implements OnInit {
 @ViewChild(Content) content: Content;
 
  configMake: any = {'placeholder': '', 'sourceField': 'VehicleMake'}
  configModel: any = {'placeholder': '', 'sourceField': 'VehicleModel'};
  formSubmitted : boolean = false;
  quoteForm: FormGroup;
  quotePlan;
  PlanTypeId;
  mmCode;
  vehicleMake = [];
  vehicleModel = [];
  serviceDoneKm = [];
  startKm = [];
  massQuote = [];
  
  constructor(private fb: FormBuilder, 
    private userSvc: UserService,
    private activatedRoute: ActivatedRoute,
    private router: Router) {
  
    this.quoteForm = this.fb.group({
      'MHRegistrationNo': [null, Validators.required],
      'MHDateOfFirstRegistration': [null, Validators.required],
      'MHMake' : [null, Validators.required],
      'MHMakeID': [null],
      'MHModel': [null, Validators.required],
      'MHModelID': [null],
      'MHCurrentKilometers': [null, Validators.required],
      'MHLastServiceDoneKilometer' : [null, Validators.required],
      'MHAverageMileagePerAnnum': [null, Validators.required],
      'MHStartKilometers': [null, Validators.required]
    });
  }

  ngOnInit() {
    this.quoteForm.controls['MHModel'].disable();
    this.quoteForm.controls['MHLastServiceDoneKilometer'].disable();

    this.activatedRoute.queryParams.subscribe(queryParams => {
      this.quotePlan = queryParams;
      this.PlanTypeId = queryParams.planID;
    });

    this.userSvc.getVehicleMakeList().subscribe(resp => {
      this.vehicleMake = resp;
    });

    if(localStorage.getItem('QuoteFormValue')) {
      let formValue = JSON.parse(localStorage.getItem('QuoteFormValue'));

      this.quoteForm.controls['MHModel'].enable();

      this.userSvc.getLastServicebyId(new HttpParams()
      .set('currentKilometers', formValue.MHCurrentKilometers)
      .set('registrationDate', formValue.MHDateOfFirstRegistration)
      .set('vehicleModelId', formValue.MHModelID)).subscribe(resp => {
        if(!resp[0] || resp[0].LastServiceDone == 0) {
          return;
        }
        this.serviceDoneKm = resp;
        formValue.MHLastServiceDoneKilometer = this.serviceDoneKm;
        this.quoteForm.controls['MHLastServiceDoneKilometer'].enable();
      });

      this.userSvc.getMileageCovered(new HttpParams()
      .set('averageKilometersPerYear', formValue.MHAverageMileagePerAnnum)
      .set('currentKm', formValue.MHCurrentKilometers)
      .set('dateOfFirstRegistration', formValue.MHDateOfFirstRegistration)
      .set('lastServiceDone', formValue.MHLastServiceDoneKilometer)
      .set('planType', this.PlanTypeId)
      .set('vehicleModelId', formValue.MHModelID)).subscribe(resp => {
        this.startKm = resp;
        formValue.MHStartKilometers = this.startKm;
      });

      this.userSvc.getVehicleModel(new HttpParams()
      .set('vehicleModelId', formValue.MHModelID))
      .subscribe(resp => {
        this.mmCode = resp.VehicleModel.MMCode;
        formValue.mmCode = this.mmCode;
      });

      this.quoteForm.patchValue(formValue);
    }
  }

  logScrolling() {
    this.content.scrollByPoint(0,100,200);
  }

  vehicleRegisterYear() {
    this.quoteForm.patchValue({
      MHMake: null,
      MHModel: null, 
      MHCurrentKilometers: null, 
      MHLastServiceDoneKilometer: null,
      MHAverageMileagePerAnnum: null,
      MHStartKilometers: null,
    });
  }

  onSelectMake(item:any) {
    this.quoteForm.patchValue({
      MHModel: null, 
      MHCurrentKilometers: null, 
      MHLastServiceDoneKilometer: null,
      MHAverageMileagePerAnnum: null,
      MHStartKilometers: null,
    });

    if(!item.VehicleMakeId || !this.quoteForm.controls['MHDateOfFirstRegistration'].value) {
      return;
    }
    this.quoteForm.get('MHMake').setValue(item.VehicleMake)
    this.quoteForm.get('MHMakeID').setValue(item.VehicleMakeId);
    this.userSvc.getVehicleModelList(new HttpParams().set('dateOfFirstRegistration', this.quoteForm.controls['MHDateOfFirstRegistration'].value)
    .set('vehicleMakeId',this.quoteForm.controls['MHMakeID'].value)).subscribe(resp => {
      this.vehicleModel = resp; 
    });
    this.quoteForm.controls['MHModel'].enable();
  }

  onSelectModel(item: any) {
    this.quoteForm.patchValue({
      MHCurrentKilometers: null, 
      MHLastServiceDoneKilometer: null,
      MHAverageMileagePerAnnum: null,
      MHStartKilometers: null,
    });
    if(!item.VehicleModelId){
      return;
    }
    this.quoteForm.get('MHModel').setValue(item.VehicleModel);
    this.quoteForm.get('MHModelID').setValue(item.VehicleModelId);
    this.userSvc.getVehicleModel(new HttpParams()
    .set('vehicleModelId',this.quoteForm.controls['MHModelID'].value))
    .subscribe(resp => {
      this.mmCode = resp.VehicleModel.MMCode;
    });
  }

  lastServiceDone(event) {
    this.quoteForm.controls['MHLastServiceDoneKilometer'].disable();
    this.serviceDoneKm = [];
    this.quoteForm.patchValue({
      MHLastServiceDoneKilometer: null,
      MHAverageMileagePerAnnum: null,
      MHStartKilometers: null,
    });
    if(!event){
      return;
    }
    this.userSvc.getLastServicebyId(new HttpParams()
    .set('currentKilometers',this.quoteForm.controls['MHCurrentKilometers'].value)
    .set('registrationDate',this.quoteForm.controls['MHDateOfFirstRegistration'].value)
    .set('vehicleModelId',this.quoteForm.controls['MHModelID'].value)).subscribe(resp => {
      if(!resp[0] || resp[0].LastServiceDone == 0) {
        return;
      }
      this.serviceDoneKm = resp;
      this.quoteForm.controls['MHLastServiceDoneKilometer'].enable();
    });
  }

  lastServiceList() {
    this.quoteForm.patchValue({
      MHAverageMileagePerAnnum: null,
      MHStartKilometers: null,
    });
  }

  AverageMileage(event) {
    this.quoteForm.patchValue({
      MHStartKilometers: null,
    });
    if(!event){
      return;
    }  
    this.userSvc.getMileageCovered(new HttpParams()
    .set('averageKilometersPerYear',this.quoteForm.controls['MHAverageMileagePerAnnum'].value)
    .set('currentKm',this.quoteForm.controls['MHCurrentKilometers'].value)
    .set('dateOfFirstRegistration',this.quoteForm.controls['MHDateOfFirstRegistration'].value)
    .set('lastServiceDone',this.quoteForm.controls['MHLastServiceDoneKilometer'].value)
    .set('planType',this.PlanTypeId)
    .set('vehicleModelId',this.quoteForm.controls['MHModelID'].value)).subscribe(resp => {
      this.startKm = resp;
    });
  }

  submit() {
    this.formSubmitted = true;
    if(!this.quoteForm.valid) {
      return
    }

    let params = {};
    params['planID'] = this.PlanTypeId;
    params['planType'] = this.quotePlan.planType;

    if(localStorage.getItem('QuoteFormValue')) {
      var formValue = JSON.parse(localStorage.getItem('QuoteFormValue'));
    }

    if(JSON.stringify(formValue) === JSON.stringify(this.quoteForm.value)) {
      this.router.navigate(['/quoteresult'], {queryParams: params});
      return;
    }
    
    this.userSvc.getMassQuoteWithReg(new HttpParams()
    .set('averageMileagePerAnnum',this.quoteForm.controls['MHAverageMileagePerAnnum'].value)
    .set('currentKilometers',this.quoteForm.controls['MHCurrentKilometers'].value)
    .set('dateOfFirstRegistration',this.quoteForm.controls['MHDateOfFirstRegistration'].value)
    .set('lastServiceDoneKilometers',this.quoteForm.controls['MHLastServiceDoneKilometer'].value)
    .set('mmCode',this.mmCode)
    .set('registrationNumber',this.quoteForm.controls['MHRegistrationNo'].value)
    .set('vehicleModelId',this.quoteForm.controls['MHModelID'].value)).subscribe(resp => {
      if(!resp[0].ID) {
        return;
      }
      this.massQuote = resp;

      if(localStorage.getItem('GuestUser')) {
        localStorage.removeItem('GuestUser');
      }

      localStorage.setItem('MassQuoteResult', JSON.stringify(this.massQuote));
      let quoteFormValue = this.quoteForm.value;
      localStorage.setItem('QuoteFormValue', JSON.stringify(quoteFormValue));
      this.router.navigate(['/quoteresult'], {queryParams: params});
    });
  }
}