import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UserService } from '../../../services/user.service';

@Component({
  selector: 'app-termsconditions',
  templateUrl: './termsconditions.page.html',
  styleUrls: ['./termsconditions.page.scss'],
})
export class TermsconditionsPage implements OnInit {

  quotePlan;
  planType;

  constructor(private activatedRoute: ActivatedRoute, private userSvc: UserService) { }

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe(queryParams => {
      this.quotePlan = queryParams;
    });

    this.userSvc.getPlanTypes().subscribe(resp => {
      var plan = resp;
      var planDetails = plan.filter(item => { return item.PlanTypeId == this.quotePlan.PlanID });
      this.planType = planDetails.map(item => { return item.PlanType });
    });
  }

}
