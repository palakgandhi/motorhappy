import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { UserService } from '../../services/user.service';
import { AppComponent } from '../../app.component';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit{

  token = localStorage.getItem('token');
  userDetail : any = {};

  constructor(private navCtrl: NavController, private userSvc: UserService, private app: AppComponent){}

  ngOnInit(){
    this.app.initializeApp();
    if(!this.token){
      return
    }
    this.userSvc.getCustomerProfile({authorisationToken:this.token}).subscribe(resp => {
      this.userDetail = resp;
    });
  }

}
