import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  cartLogin;
  view : any;
  OTPForm : FormGroup;
  passwordForm : FormGroup;
  passwordDetail : any;
  formSubmitted : boolean = false;
  public type = 'password';
  public showPass = false;

  constructor(private fb: FormBuilder, private userSvc: UserService, private router: Router, private activatedRoute: ActivatedRoute ) {
    this.OTPForm = this.fb.group({
      'type': ['id'],
      'identificationNo': [null, [Validators.required,Validators.minLength(13), Validators.maxLength(13)]],
      'passportNo': [null, []],
    });
    this.passwordForm = this.fb.group({
      'type': ['id'],
      'identificationNo': [null, [Validators.required,Validators.minLength(13), Validators.maxLength(13)]],
      'passportNo': [null, []],
      'password': [null, Validators.required]
    });
   }

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe(queryParams => {
      if(!queryParams.cart)
        return;
      this.cartLogin = queryParams;
    });
   }

  showPassword() {
    this.showPass = !this.showPass;
 
    if(this.showPass){
      this.type = 'text';
    } else {
      this.type = 'password';
    }
  }

  changeForm(){
    this.formSubmitted = false;
  }
  
  segmentChanged(event){
    this.view = event.detail.value;
  }

  onTypeChangeOTP(event) {
    if (event == 'id') {
      this.OTPForm.get('identificationNo').setValidators([Validators.required]);
      this.OTPForm.get('passportNo').setValidators(null);
      this.OTPForm.get('passportNo').setValue(null);

    } else {
      this.OTPForm.get('passportNo').setValidators([Validators.required]);
      this.OTPForm.get('identificationNo').setValidators(null);
      this.OTPForm.get('identificationNo').setValue(null);

    }

    this.OTPForm.updateValueAndValidity();
  }

  onTypeChangePassword(event) {
    if (event == 'id') {
      this.passwordForm.get('identificationNo').setValidators([Validators.required]);
      this.passwordForm.get('passportNo').setValidators(null);
      this.passwordForm.get('passportNo').setValue(null);
    } else {
      this.passwordForm.get('passportNo').setValidators([Validators.required]);
      this.passwordForm.get('identificationNo').setValidators(null);
      this.passwordForm.get('identificationNo').setValue(null);
    }

    this.passwordForm.updateValueAndValidity();
  }

  validFormData(payload){
    if(payload.type == 'id') {
      delete payload.passportNo;
    }
    if(payload.type == 'passport') {
      delete payload.identificationNo;
    }
    delete payload.type;
  }

  loginwithPassword() {
    this.formSubmitted = true;
    let payload = this.passwordForm.value;
    this.validFormData(payload);
    if(this.passwordForm.valid){
      this.userSvc.loginwithPassword(payload).subscribe(resp => {
        if(resp.isVerified == false){
          let params = {};
          params['isVerified'] = false;
          if (payload.type === 'id') {
            params['identificationNo'] = payload.identificationNo;
          } else {
            params['passportNo'] = payload.passportNo;
          }
          this.router.navigate(['/accountverification'], {queryParams: params});
        } 
        else if(resp.authorisationToken && resp.isVerified == true){
          this.passwordDetail = resp;
          localStorage.setItem('token', this.passwordDetail.authorisationToken);

          if(this.cartLogin){
            this.router.navigate(['/cartpersonaldetail']);
            return;
          }
          
          this.router.navigate(['/home']);
        }
      });
    }
  }

  loginwithOTP() {
    this.formSubmitted = true;
    let payload = this.OTPForm.value;
    this.validFormData(payload);
    if(this.OTPForm.valid){
      this.userSvc.loginwithOTP(payload).subscribe(resp => {
        if(resp == null) {
          let params = {};
          if (payload.identificationNo) {
            params['identificationNo'] = payload.identificationNo;
          }
          else {
            params['passportNo'] = payload.passportNo;
          }
    
          if(this.cartLogin) {
            params['cars'] = true;
            this.router.navigate(['/accountverification'], {queryParams: params});
            return;
          }
          this.router.navigate(['/accountverification'], {queryParams: params});
        }
      });
    }
  }
  
}
