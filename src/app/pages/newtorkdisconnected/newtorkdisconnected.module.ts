import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { NewtorkdisconnectedPage } from './newtorkdisconnected.page';

const routes: Routes = [
  {
    path: '',
    component: NewtorkdisconnectedPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [NewtorkdisconnectedPage]
})
export class NewtorkdisconnectedPageModule {}
