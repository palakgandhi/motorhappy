import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewtorkdisconnectedPage } from './newtorkdisconnected.page';

describe('NewtorkdisconnectedPage', () => {
  let component: NewtorkdisconnectedPage;
  let fixture: ComponentFixture<NewtorkdisconnectedPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewtorkdisconnectedPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewtorkdisconnectedPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
