import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { FormValidators } from '../../services/formvalidators';
import { CustomValidators } from 'ng2-validation';
import { NavController, ToastController } from '@ionic/angular';
import * as moment from 'moment';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {

  token = localStorage.getItem('token');
  profileDetailForm : FormGroup;
  contactDetailsForm : FormGroup;
  postalAddressForm : FormGroup;
  OTPpreferencesForm : FormGroup;
  loginDetailForm : FormGroup;
  formSubmitted : boolean = false;
  userDetail : any = {};
  contactDetails : any = {};
  postalAddress : any = {};
  OTPpreferenceDetail : any = {};
  postCodeDesc : any = [];
  view : any;
  title : any;
  suburbList: any;
  public oldpasswordType = 'password';
  public passwordType = 'password';
  public confirmPasswordType = 'password';

  constructor(
    private userSvc: UserService,
    private fb: FormBuilder,
    private navCtrl: NavController,
    public toastCtrl: ToastController) 
  { 
    let email = new FormControl('', [Validators.required, Validators.email]);
    let password = new FormControl('', [Validators.required, FormValidators.password]);
    this.profileDetailForm = this.fb.group({
      'TitleId': [null, Validators.required],
      'firstName' : [null, Validators.required],
      'surname': [null, Validators.required],
      'birthDate': [null, [Validators.required, FormValidators.dateCompare]],
      'identificationNo': [null],
      'passportNo': [null],
    });
    this.contactDetailsForm = this.fb.group({
      'cellNumber':[null, [Validators.required, FormValidators.phoneNumber]],
      'email': email,
      'homeNumber': [null, FormValidators.phoneNumber],
      'workNumber': [null, FormValidators.phoneNumber]
    });
    this.postalAddressForm = this.fb.group({
      'addressLine1': [null, Validators.required],
      'addressLine2': [null, Validators.required],
      'addressLine3': [null, Validators.required],
      'Suburb': [null, Validators.required]
    });
    this.OTPpreferencesForm = this.fb.group({
      'textMessageOTPDestination': [null, [Validators.required, FormValidators.phoneNumber]],
      'emailMessageOTPDestination': email
    });
    this.loginDetailForm = this.fb.group({
      'oldPassword': [null,[Validators.required, FormValidators.password]],
      'newPassword': password,
      'confirmPassword': [null, [Validators.required, CustomValidators.equalTo(password)]],
    });
  }

  ngOnInit() {
    if(!this.token){
      return
    }
    this.userSvc.getCustomerProfile({authorisationToken:this.token}).subscribe(resp => {
      this.userDetail = resp.personalDetails;
      this.contactDetails = resp.contactDetails;
      this.OTPpreferenceDetail = resp.oTPDestinations;
      this.userDetail.birthDate = this.userDetail.birthDate ? moment(this.userDetail.birthDate).format('YYYY-MM-DD') : null;
      this.profileDetailForm.patchValue(this.userDetail);
      this.contactDetailsForm.patchValue(this.contactDetails);
      this.OTPpreferencesForm.patchValue(this.OTPpreferenceDetail);
      this.postalAddress = resp.postalAddress;
      if(this.postalAddress.postCodeDesc != ''){
        this.userSvc.getSingleSuburb(this.postalAddress.postCodeDesc).subscribe(resp => {
           this.suburbList = resp.value;
           this.postalAddress.Suburb = this.suburbList;
           this.postalAddressForm.patchValue(this.postalAddress);
       });
      }
    });
    this.userSvc.getTitle().subscribe(resp => {
      this.title = resp;
    });
  }

  getPostCodeData(event){
    let Suburb = event;
    this.userSvc.getSuburbList(Suburb).subscribe(resp => {
      this.postCodeDesc = resp.value;
      this.postalAddressForm.patchValue(this.postCodeDesc);
    });
  }

  segmentChanged(event){
    this.view = event.detail.value;
  }

  maxDateString(){
    return new Date().toISOString().split('T')[0];
  }

  showPassword(field){
    this[field] = this[field] == 'password' ? 'text' : 'password';
  }

  changeSubmit(){
    this.formSubmitted = false;
  }

  UpdateProfileDetails(){
    this.formSubmitted = true;
    if(!this.profileDetailForm.valid){
      return;
    }
    let payload = {};
    payload['customerData'] = this.profileDetailForm.value;
    payload['authorisationToken'] = this.token;
    this.userSvc.updatePersonalDetails(payload).subscribe(resp => {
      if(resp.customerId){
        this.navCtrl.navigateRoot(['/verifyupdates']);
      }
    });
  }

  UpdateContactDetails(){
    this.formSubmitted = true;
    if(!this.contactDetailsForm.valid){
      return;
    }
    let payload = {};
    payload['newContactDetails'] = this.contactDetailsForm.value;
    payload['authorisationToken'] = this.token;
    this.userSvc.updateContactDetails(payload).subscribe(resp => {
      if(resp.customerId){
        this.navCtrl.navigateRoot(['/verifyupdates']);
      }
    });
  }

  UpdatepostalAddress(){
    this.formSubmitted = true;
    if(!this.postalAddressForm.valid){
      return;
    }
    let payload = {};
    payload['authorisationToken'] = this.token;
    payload['newPostalAddress'] = this.postalAddressForm.value;   
    payload['newPostalAddress'].postCode = this.postalAddressForm.value.Suburb[0].ID;
    payload['newPostalAddress'].provinceId = this.postalAddressForm.value.Suburb[0].Code;
    delete payload['newPostalAddress'].Suburb;
    this.userSvc.updatePostalAddress(payload).subscribe(resp => {
      if(resp.customerId){
        this.navCtrl.navigateRoot(['/verifyupdates']);
      }
    });
  }

  UpdateOTPDetail(){
    this.formSubmitted = true;
    if(!this.OTPpreferencesForm.valid){
      return;
    }
    let payload = {};
    payload['authorisationToken'] = this.token;
    payload['customerData'] = this.OTPpreferencesForm.value;
    this.userSvc.updateOTPDetails(payload).subscribe(resp => {
      if(resp.customerId){
        this.navCtrl.navigateRoot(['/verifyupdates']);
      }
    });
  }

  UpdateChangePassword(){
    this.formSubmitted = true;
    if(!this.loginDetailForm.valid){
      return;
    }
    let formdata = this.profileDetailForm.value;
    let payload = this.loginDetailForm.value;
    payload.identificationNo = formdata.identificationNo;
    payload.passportNo = formdata.passportNo;
    this.userSvc.changesPassword(payload).subscribe(async resp => {
      if(resp == null){
        this.formSubmitted = false;
        this.loginDetailForm.reset();
        const toast = await this.toastCtrl.create({
              message: 'successfully updated Your password',
              duration: 2000
        });
        toast.present();
      }
    });
  }
}
