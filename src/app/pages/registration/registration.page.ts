import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { FormValidators } from '../../services/formvalidators';
import { CustomValidators } from 'ng2-validation';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.page.html',
  styleUrls: ['./registration.page.scss'],
})
export class RegistrationPage implements OnInit {

  registerForm : FormGroup;
  formSubmitted : boolean = false;
  title : any = [];
  public passwordType = 'password';
  public confirmPasswordType = 'password';
  
  constructor(private fb: FormBuilder, private userSvc: UserService, private router: Router) 
  { 
    let email = new FormControl('', [Validators.required, Validators.email]);
    let password = new FormControl('', [Validators.required, FormValidators.password]);
    this.registerForm = this.fb.group({
      'type': ['id'],
      'identificationNo': [null, [Validators.required,Validators.minLength(13), Validators.maxLength(13)]],
      'passportNo': [null, []],
      'email': email,
      'confirmEmail': [null, [Validators.required , FormValidators.emailCompare(email)]],
      'password': password,
      'confirmPassword': [null, [Validators.required, CustomValidators.equalTo(password)]],
      'titleId': [null, Validators.required],
      'firstname' : [null, Validators.required],
      'surname': [null, Validators.required],
      'dateOfBirth': [null, [Validators.required, FormValidators.dateCompare]],
      'cellphoneNo': [null, [Validators.required, FormValidators.phoneNumber]],
      'acceptTermsAndCondition': [true],
      'emailVerified': [true]
    });
  }

  ngOnInit() {
    this.userSvc.getTitle().subscribe(resp => {
      this.title = resp;
    });
   }

  showPassword(field) {
    this[field] = this[field] == 'password' ? 'text' : 'password';
  }

  changeForm(){
    this.formSubmitted = false;
  }

  onTypeChangeOTP(event) {
    if (event == 'id') {
      this.registerForm.get('identificationNo').setValidators([Validators.required]);
      this.registerForm.get('passportNo').setValidators(null);
      this.registerForm.get('passportNo').setValue(null);

    } else {
      this.registerForm.get('passportNo').setValidators([Validators.required]);
      this.registerForm.get('identificationNo').setValidators(null);
      this.registerForm.get('identificationNo').setValue(null);

    }

    this.registerForm.updateValueAndValidity();
    }

  maxDateString(){
    return new Date().toISOString().split('T')[0];
  }

  validFormData(payload){
    if(payload.type == 'id') {
      delete payload.passportNo;
    }
    if(payload.type == 'passport') {
      delete payload.identificationNo;
    }
    delete payload.type;
  }

  Register(){
    this.formSubmitted = true;
    if(!this.registerForm.valid){
      return;
    }
    let payload = this.registerForm.value;
    this.validFormData(payload);
    this.userSvc.register(payload).subscribe(resp => {
      if(resp == null){
        let params = {};
        if (payload.type == 'id')
          params['identificationNo'] = payload.identificationNo;
        else
          params['passportNo'] = payload.passportNo;

        this.router.navigate(['/accountverification'], {queryParams: params});
      }
    });
  }
}
