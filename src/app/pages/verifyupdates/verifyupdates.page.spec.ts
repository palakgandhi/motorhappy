import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerifyupdatesPage } from './verifyupdates.page';

describe('VerifyupdatesPage', () => {
  let component: VerifyupdatesPage;
  let fixture: ComponentFixture<VerifyupdatesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerifyupdatesPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerifyupdatesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
