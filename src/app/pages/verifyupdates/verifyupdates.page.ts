import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-verifyupdates',
  templateUrl: './verifyupdates.page.html',
  styleUrls: ['./verifyupdates.page.scss'],
})
export class VerifyupdatesPage implements OnInit {

  token = localStorage.getItem('token');
  OTPForm : FormGroup;
  formSubmitted : boolean = false;

  constructor(private userSvc: UserService, private fb: FormBuilder, private navCtrl: NavController) {
    this.OTPForm = this.fb.group({
      'oneTimePassword': [null, Validators.required]
    })
   }

  ngOnInit() { }

  verifyOTP(){
    this.formSubmitted = true;
    if(!this.OTPForm.valid){
      return;
    }
    let payload;
    payload = this.OTPForm.value;
    payload['authorisationToken'] = this.token;
    this.userSvc.ConfirmCustomerUpdates(payload).subscribe(resp => {
      if(resp == null){
        this.navCtrl.goBack();
      }
    });
  }
}
