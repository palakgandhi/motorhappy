import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'dateFormat'
})
export class DateFormatPipe implements PipeTransform {

  transform(value: any, format?: any): any {
    if (!value) {
      return null;
    }

    let date;
    if (typeof value == 'string') {
      date = value;
    } else {
      date = value.date;
    }

    if (date.indexOf('.') != -1)
      date = date.replace(/-/g, '/').substr(0, date.indexOf('.'));

    let defaultFormat = format || 'DD MMM YYYY';
    return moment(date).format(defaultFormat);
  }
}
