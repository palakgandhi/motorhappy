import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { LoadingService } from './services/loading.service';

@Injectable()
export class ProgressInterceptor implements HttpInterceptor {
  constructor(private loaderSvc: LoadingService) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (request.headers.has('ignoreLoadingBar')) {
      return next.handle(request.clone({ headers: request.headers.delete('ignoreLoadingBar') }));
    }
    this.loaderSvc.startLoading();
    return next.handle(request).pipe(finalize(() => this.loaderSvc.stopLoading()));
  }
}
