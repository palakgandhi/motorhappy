import { Injectable } from '@angular/core';
import { ReplaySubject } from 'rxjs'

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  public isLoginSubject = new ReplaySubject<boolean>(0);
  public isLoggedin = this.isLoginSubject.asObservable();
  
  constructor() { }

  public isAuthenticated(): boolean {

		let currentUser = localStorage.getItem('token');
		if(currentUser){
      return true;
		} else {
      return false;
    }
	}
}

