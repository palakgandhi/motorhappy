import { AbstractControl, ValidatorFn } from '@angular/forms';

export class FormValidators {
  static password(control: AbstractControl) {
    if (!new RegExp(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{8,}/g).test(control.value)) {
      return {invalidPassword: true};
    }

    return null;
  }

  static dateCompare(compareWith: AbstractControl): ValidatorFn {
    let subscribe: boolean = false;
    return (control: AbstractControl): {[key: string]: boolean} => {
      if (!subscribe) {
        subscribe = true;
        compareWith.valueChanges.subscribe(() => {
          control.updateValueAndValidity();
        });
      }

      let startDate = compareWith.value;
      let endDate = control.value;
      if (!startDate || !endDate) {
        return null;
      }

      if (startDate.epoc && endDate.epoc) {
        return endDate.epoc < startDate.epoc ? {smallerEndDate: true} : null;
      }

      return null;
    };
  }

  static phoneNumber(control: AbstractControl) {
    if (!control.value)
      return null;

    let value = control.value.replace(/[ _]/g, '');
    if (!new RegExp(/^(0)(\d{9})$/g).test(value)) {
      return { invalidPhoneNumber: true };
    }

    return null;
  }

  static emailCompare(compareWith: AbstractControl): ValidatorFn {
    let subscribe: boolean = false;
    return (control: AbstractControl): {[key: string]: boolean} => {
      if (!subscribe) {
        subscribe = true;
        compareWith.valueChanges.subscribe(() => {
          control.updateValueAndValidity();
        });
      }

      let email = compareWith.value;
      let confirm_email = control.value;
      if (!email || !confirm_email) {
        return null;
      }

      return (email !== confirm_email) ? {equalToEmail: true} : null;
    };
  }
}