import { Injectable } from '@angular/core';
import { Observable , of  } from 'rxjs';
import { catchError } from 'rxjs/operators'
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { AlertController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(private http : HttpClient, private alertCtrl: AlertController ) { }

  get(url: string, httpParams = new HttpParams(), headers = new HttpHeaders()): Observable<any>{
    return this.http.get(url, {params: httpParams, headers: headers}).pipe(
    catchError(err => this.handleErrors(err)));
  }
  post(url: string, payload: Object = {}): Observable<any>{
    return this.http.post(url,payload).pipe(
    catchError(err => this.handleErrors(err)));
  }
  patch(url: string, payload: Object = {}): Observable<any>{
    return this.http.patch(url,payload).pipe(
    catchError(err => this.handleErrors(err)));
  }
  delete(url: string): Observable<any>{
    return this.http.delete(url).pipe(
    catchError(err => this.handleErrors(err)));
  }

  getBlob(url: string, payload: Object = {}, params: HttpParams = new HttpParams()): Observable<any> {
    return this.http.post(url, payload, {params: params, responseType: 'blob'}).pipe(
      catchError(err => this.handleErrors(err)));
  }

  private handleErrors(err: Response): Observable<any> {
    switch (err.status) {
        case 400:
            // Bad Request
            this.presentToast(err['error'].Error.ErrorMessage);
            return of(err['error'].Error.ErrorMessage);

        case 401:
            // Unauthorized Access
            this.presentToast(err['error'].Error.ErrorMessage);
            return of(err['error'].Error.ErrorMessage);

        case 403:
            // Forbidden
            this.presentToast(err['error'].Error.ErrorMessage);
            return of(err['error'].Error.ErrorMessage);

        case 404:
            // Resource Not Found
            this.presentToast(err['error'].Error.ErrorMessage);
            return of(err['error'].Error.ErrorMessage);
            // return err['error'].error;

        case 422:
            // Resource Not Found
            Object.keys(err['error'].errors).forEach(key => {
                this.presentToast(err['error'].errors[key][0]);
            });
            return of(err['error'].error.message);
            // break;

        case 429:
            // Too Many Requests
            break;

        case 500:
            // Internal Server Error
            this.presentToast(err['error'].Error.ErrorMessage);
            return of(err['error'].Error.ErrorMessage);
    }

    return of(err);
    }

    private async presentToast(message) {
        const alert = await this.alertCtrl.create ({
            message: message,
            cssClass: 'alert-error-msg'
        });

        alert.present();
        setTimeout(() => { alert.dismiss(); }, 3000);
    }

}
