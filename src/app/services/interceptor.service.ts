import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable, EMPTY } from 'rxjs';
import { NetworkService } from './network.service';

@Injectable()
export class InterceptorService implements HttpInterceptor {

  constructor(public network: NetworkService){}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (!this.network.checkConnection()) {
      return EMPTY;
    } else {
      const authReq = request.clone({
        setHeaders: {
          Authorization: "Basic "+btoa("MotorHappy:MotorHappy")
        }
      });

      return next.handle(authReq);
    }
  } 
}
