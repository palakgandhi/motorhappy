import { Injectable } from '@angular/core';
import { Network } from '@ionic-native/network/ngx';
import { NavController, ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class NetworkService {

  private connection : boolean = true;
  
  constructor(private network: Network, private navCtrl: NavController, private toast: ToastController) {
    this.network.onDisconnect().subscribe(() => {
      this.connection = false;   
      this.navCtrl.navigateRoot('/newtorkdisconnected');
      this.toastIt('No Internet Connection');
    });
    this.network.onConnect().subscribe(() => {
      this.connection = true;
      this.navCtrl.navigateBack('/home');
    });
   }

   public checkConnection() {
    if (!this.connection) {
      return false;
    }
    return true;
  }

  async toastIt(message: string) {
    let toastCtrl = await this.toast.create({
      message: message,
      duration: 2000,
      position: 'bottom',
    });
    toastCtrl.present();
  }
}
