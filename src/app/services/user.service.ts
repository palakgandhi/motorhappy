import { Injectable } from '@angular/core';
import { HttpService } from '../services/http.service';
import { HttpHeaders } from '@angular/common/http';

const apiUrl = 'https://peqa.liquidcapital.co.za/PinkElephant/';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private httpSvc : HttpService) { }

  loginwithPassword(payload) {
    return this.httpSvc.post(apiUrl + 'api/SecurityService/Login' , payload);
  }

  loginwithOTP(payload) {
    return this.httpSvc.post(apiUrl + 'api/SecurityService/LoginWithOTP', payload);
  }

  forgotPassword(payload){
    return this.httpSvc.post(apiUrl + 'api/SecurityService/ResetPassword', payload);
  }

  resetPasswordverification(payload) {
    return this.httpSvc.post(apiUrl + 'api/SecurityService/ResetPasswordVerification', payload);
  }

  register(payload) {
    return this.httpSvc.post(apiUrl + 'api/SecurityService/Register', payload);
  }

  verifyRegister(payload) {
    return this.httpSvc.post(apiUrl + 'api/SecurityService/RegisterVerification', payload);
  }

  resendVerificationRegister(payload) {
    return this.httpSvc.post(apiUrl + 'api/SecurityService/RegisterResendVerification', payload);
  }

  getCustomerProfile(token) {
    return this.httpSvc.post(apiUrl + 'api/Profile/GetCustomer', token);
  }

  getVehicles(params) {
    return this.httpSvc.post(apiUrl + 'api/Vehicle/GetVehicles', params);
  }

  getTitle() {
    return this.httpSvc.get(apiUrl + 'api/EnumLookup/GetCustomerTitles');
  }

  updatePersonalDetails(payload) {
    return this.httpSvc.post(apiUrl + 'api/Profile/UpdateCustomerPersonalDetails', payload);
  }

  updateContactDetails(payload) {
    return this.httpSvc.post(apiUrl + 'api/Profile/UpdateCustomerContactDetailRequest', payload);
  }

  getCustomerProvice() {
    return this.httpSvc.get(apiUrl + 'api/EnumLookup/GetProvincesEnumValues');
  }

  getSuburbList(Suburb: string) {
    return this.httpSvc.get(apiUrl + 'PostCodeDataService.svc/PostCodeData?$filter=substringof(%27Street%27,PostCodeStyle)%20and%20substringof(%27'+Suburb.toUpperCase()+'%27,Suburb)&$skip=0&$orderby=Suburb&$format=json');
  }

  getSingleSuburb(Suburb: string) {
    return this.httpSvc.get(apiUrl + 'PostCodeDataService.svc/PostCodeData?$filter=substringof(%27Street%27,PostCodeStyle)%20and%20substringof(%27'+encodeURI(Suburb)+'%27,Name)&$skip=0&$orderby=Suburb&$format=json');
  }

  updatePostalAddress(payload) {
    return this.httpSvc.post(apiUrl +'api/Profile/UpdateCustomerPostalAddress', payload);
  }

  updateOTPDetails(payload) {
    return this.httpSvc.post(apiUrl + 'api/Profile/UpdateCustomerOTPDetailsRequest', payload);
  }

  ConfirmCustomerUpdates(payload) {
    return this.httpSvc.post(apiUrl + 'api/Profile/ConfirmCustomerUpdates', payload);
  }

  changesPassword(payload) {
    return this.httpSvc.post(apiUrl + 'api/SecurityService/ChangePassword', payload);
  }

  getDocument(payload) {
    return this.httpSvc.getBlob(apiUrl + 'api/Policy/GetPolicyDocument', payload);
  }

  sendDocument(payload) {
    return this.httpSvc.post(apiUrl + 'api/Policy/ResendCustomerOfferDocumentation', payload);
  }

  getPlanTypes() {
    return this.httpSvc.get(apiUrl + 'api/QuoteReferenceData/GetPlanTypes');
  }

  getVehicleModel(params) {
    return this.httpSvc.get(apiUrl + 'api/QuoteReferenceData/GetVehicleModel?', params);
  }

  getVehicleMakeList() {
    return this.httpSvc.get(apiUrl + 'api/QuoteReferenceData/GetVehicleMakeList');
  }

  getVehicleModelList(params) {
    return this.httpSvc.get(apiUrl + 'api/QuoteReferenceData/GetVehicleModelList?', params);
  }
  
  getLastServicebyId(params) {
    return this.httpSvc.get(apiUrl + 'api/Quote/GetLastServiceDoneByVehicleModelId?', params , 
    new HttpHeaders().set('ignoreLoadingBar', 'true'));
  }

  getMileageCovered(params) {
    return this.httpSvc.get(apiUrl + 'api/Quote/GetStartMileageCovered?', params , 
    new HttpHeaders().set('ignoreLoadingBar', 'true'));
  }

  getMassQuotes(params) {
    return this.httpSvc.get(apiUrl + 'api/Quote/GetMassQuotes?', params);
  }

  getMassQuoteWithReg(params) {
    return this.httpSvc.get(apiUrl + 'api/Quote/GetMassQuotesWithReg', params);
  }

  getQuoteImage(params) {
    return this.httpSvc.get(apiUrl + 'api/Quote/GetQuoteImage', params);
  }

  getProductYearCover(params) {
    return this.httpSvc.get(apiUrl + 'api/Quote/GetProductYearToCover', params);
  }

  getProductMileage(params) {
    return this.httpSvc.get(apiUrl + 'api/Quote/GetProductMileage', params);
  }

  CreateLead(payload) {
    return this.httpSvc.post(apiUrl + 'api/Lead/CreateLead', payload);
  }

  LeadSubmission(payload) {
    return this.httpSvc.post(apiUrl + 'api/Lead/LeadSubmission', payload);
  }

  lead(payload) {
    return this.httpSvc.post('https://test.motorhappy.co.za/api/CallMeBack' , payload);
  }

  leadBasketQuoteAdd(payload) {
    return this.httpSvc.post(apiUrl + 'api/Lead/LeadBasketQuoteAdd' , payload);
  }

  leadBasketGet(payload) {
    return this.httpSvc.post(apiUrl + 'api/Lead/LeadBasketGet' , payload);
  }

  leadBasketQuoteRemove(payload) {
    return this.httpSvc.post(apiUrl + 'api/Lead/LeadBasketQuoteRemove' , payload);
  }

  customerActionUpdate(payload) {
    return this.httpSvc.post(apiUrl + 'api/CustomerAction/CustomerActionUpdate', payload);
  }

  copyLeadToCustomerAction(payload) {
    return this.httpSvc.post(apiUrl + 'api/CustomerAction/CopyLeadToCustomerAction', payload);
  }

  customerUpdate(payload) {
    return this.httpSvc.post(apiUrl + 'api/CustomerAction/CustomerUpdate', payload);
  }

  baskedQuoteAdd(payload) {
    return this.httpSvc.post(apiUrl + 'api/CustomerAction/BasketQuoteAdd', payload);
  }

  basketGet(payload) {
    return this.httpSvc.post(apiUrl + 'api/CustomerAction/BasketGet', payload);
  }

  basketQuoteRemove(payload) {
    return this.httpSvc.post(apiUrl + 'api/CustomerAction/BasketQuoteRemove', payload);
  }

  getPaymentMethod() {
    return this.httpSvc.get(apiUrl + 'api/EnumLookup/GetPaymentMethodEnumValues');
  }

  getBankAccountType() {
    return this.httpSvc.get(apiUrl + 'api/EnumLookup/GetBankAccountTypeEnumValues');
  }
  
  debitBankAccountDetailsUpdate(payload) {
    return this.httpSvc.post(apiUrl + 'api/CustomerAction/CustomerActionBankAccountDetailsUpdate', payload);
  }

  quoteUpdateWithVehicleRegistrationNo(payload) {
    return this.httpSvc.post(apiUrl + 'api/CustomerAction/QuoteUpdateWithVehicleRegistrationNo', payload)
  }

  customerActionSubmit(payload) {
    return this.httpSvc.post(apiUrl + 'api/CustomerAction/CustomerActionSubmit', payload);
  }
  // contactDealer(payload){
  //   return this.httpSvc.post('https://cars.motorhappy.co.za/ajax_process_request.php', payload);
  // }
}


