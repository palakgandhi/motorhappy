import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ErrorComponent } from '../component/error/error.component';
import { LoadingComponent } from '../component/loading/loading.component';
import { LookupComponent } from '../component/lookup/lookup.component';
import { KeysPipe } from '../pipe/keys.pipe';
import { DateFormatPipe } from '../pipe/date-format.pipe';

@NgModule({
  declarations: [ErrorComponent, LoadingComponent, LookupComponent, KeysPipe, DateFormatPipe],
  imports: [
    CommonModule,
    FormsModule
  ],
  exports: [ErrorComponent, LoadingComponent, LookupComponent, KeysPipe, DateFormatPipe]
})

export class SharedModule {
  static forRoot(): ModuleWithProviders {
      return {
          ngModule: SharedModule
      };
  }
}
